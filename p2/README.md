> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Alex Gamez

### Project 2 Requirements:

*Six Parts*

1. Edit button, delete button and RSS feed code development.
2. Screenshot of before/after successful edit.
3. Screenshot of failed/passed server-side validation.
4. Screenshot of deleted prompt and successfully deleted record.
5. Screenshot of RSS feed.
6. Link to local lis4381 web app: [Local Host AMPPS](http://localhost:8080/repos/lis4381/)


### Project Screenshots:

*Before/During Edit*

| Before Edit | During Edit |
| ---------- | ---------- |
| ![Before Edit](img/before_edit.png) | ![During Edit](img/during_edit.png) |

*Failed/Passed Server-Side Validation*

| Failed validation | Passed validation |
| ---------- | ---------- |
| ![Failed validation](img/failed_v.png) | ![Passed validation](img/passed_v.png) |

*Deleted Prompt and Successfully Deleted Record*

| Deleted Prompt | Deleted Record |
| ---------- | ---------- |
| ![Deleted prompt](img/deleted_prompt.png) | ![Successfully deleted record](img/deleted_record.png) |

*RSS Feed*

![RSS Feed](img/rss.png)


### Links

Local LIS4381 Web App: [Local Host](http://localhost:8080/repos/lis4381/)

