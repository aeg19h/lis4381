> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Alex Gamez

### Assignment 5 Requirements:

*Seven Parts*

1. Screenshot of Assignment 5 PHP Page (index.php)
2. Screenshot of invalid petstore addition (add_petstore.php)
3. Screenshot of valid petstore addition (add_petstore.php)
4. Screenshot of failed validation for adding a petstore (add_petstore_process.php)
5. Screenshot of passed validation for adding a petstore (add_petstore_process.php)
6. Link to local lis4381 web app: [Local Host AMPPS](http://localhost:8080/repos/lis4381/)
7. Screenshot of skillsets 13-15.


### Assignment Screenshots:

*Screenshot of A5 index.php*

![a5 index.php](img/index.png)

*Screenshot of add_petstore.php (invalid)*

![add_petstore.php invalid](img/petstore_iv.png)

*Screenshot of Failed Server-Side Validation*

![Failed Server-Side Validation](img/failed_ssv.png)

*Screenshot of add_petstore.php (valid)*

![add_petstore.php valid](img/petstore_v.png)

*Screenshot of Passed Server-Side Validation*

![Passed Server-Side Validation](img/passed_ssv.png)


### Links

Local LIS4381 Web App: [Local Host](http://localhost:8080/repos/lis4381/)

#### Skillset 13-15 Screenshots:

*Screenshot of Skillset 13: Sphere_Volume_Calculator*

![Skillset 13 Sphere_Volume_Calculator Screenshot](img/q13_Sphere_Volume_Calculator.png)

*Screenshots of Skillset 14_PHP: Simple Calculator*

| Screenshot of Add Index | Screenshot of Add Process |
| ---------- | ---------- |
| ![Add Index SS](img/add1.png) | ![Add Process SS](img/add2.png) |

| Screenshot of Division Index| Screenshot of Division Process |
| ---------- | ---------- |
| ![Division Index SS](img/divide1.png) | ![Division Process SS](img/divide2.png) |

*Screenshots of Skillset 15_PHP: Write/Read File*

| Screenshot of Write/Read Index| Screenshot of Write/Read Process |
| ---------- | ---------- |
| ![Write/Read Index SS](img/write_read1.png) | ![Write/Read Process SS](img/write_read2.png) |


