import java.util.Scanner;

public class Methods
{
    public static void getRequirements()
    {
        System.out.println("\nDeveloper: Alex Gamez");
        System.out.println("Program determines wheher user-entered value is alpha, numeric, or special character.");
        System.out.println("");
        System.out.println("References:");
        System.out.println("ASCII Baskground: https://en.wikipedia.org/wiki/ASCII");
        System.out.println("ASCII Character Table: https://wwww.ascii-code.com/");
        System.out.println("Lookup Tables: https://wwww.lookuptables.com/");
        System.out.println("");
    }

    public static void determineChar()
    {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter character: ");
        char ch = sc.next().charAt(0);

        if((ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z'))
        {
            System.out.println(ch + "is alpha. ASCII value: " + (int) ch);
        }
        else if(ch >= '0' && ch <= '9')
        {
            System.out.println(ch + "is numeric. ASCII value: " + (int) ch);
        }
        else 
        {
            System.out.println(ch + "is specia; character. ASCII value: " + (int) ch);
        }

    }
}
