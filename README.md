> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS4381 - Mobile Web Application Development

## Alex Gamez

### LIS4381 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install AMPPS
    - Install JDK
    - Install Android Studio and create My First App
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorials (bitbucketstationlocations and myteamquotes)
    - Provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
	- Create a mobile recipe app using Android Studio.
	- Screenshot of running application’s first user interface.
	- Screenshot of running application’s second user interface.
	- Screenshot of Skillsets 1-3.
	
3. [A3 README.md](a3/README.md "My A3 README.md file")
	- Create a (design) database solutions that interact with a web application.
	- Create a mobile app per the requirements.
	- Research in creation of a icon image, border around ImageView and Button and text shadow.
	- Screenshots of Skillsets
	
3. [A4 README.md](a4/README.md "My A4 README.md file")
	- Screenshot of LIS4381 Portal (Main Page)
	- Screenshot of Failed Validation
	- Screenshot of Passed Validation
	- Link to local lis4381 web app: [Local Host AMPPS](http://localhost:8080/repos/lis4381/)
	- Screenshot of Skillsets 10-12
	
4. [A5 README.md](a5/README.md "My A5 README.md file")
	- Screenshot of Assignment 5 PHP Page (index.php)
	- Screenshot of invalid petstore addition (add_petstore.php)
	- Screenshot of valid petstore addition (add_petstore.php)
	- Screenshot of failed validation for adding a petstore (add_petstore_process.php)
	- Screenshot of passed validation for adding a petstore (add_petstore_process.php)
	- Link to local lis4381 web app: [Local Host AMPPS](http://localhost:8080/repos/lis4381/)
	- Screenshot of skillsets 13-15.
	
5. [P1 README.md](p1/README.md "My P1 README.md file")
	- Screenshot of running application’s first user interface
	- Screenshot of running application’s second user interface
	- Research how to display launcher icon on action bar, change background color, add image border and text shadow to button.
	- Questions (PHP/MySQL: Chs. 7, 8)
	- Screenshot of skillsets 7-9.
	
6. [P2 README.md](p2/README.md "My P2 README.md file")
	- Edit button, delete button and RSS feed code development.
	- Screenshot of before/during an edit.
	- Screenshot of failed/passed server-side validation.
	- Screenshot of deleted prompt and successfully deleted record.
	- Screenshot of RSS feed.
	- Link to local lis4381 web app: [Local Host AMPPS](http://localhost:8080/repos/lis4381/)

	

