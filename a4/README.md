> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Alex Gamez

### Assignment 4 Requirements:

*Six Parts*

1. Screenshot of LIS4381 Portal (Main Page)
2. Screenshot of Failed Validation
3. Screenshot of Passed Validation
5. Link to local lis4381 web app: [Local Host](http://localhost:8080/repos/lis4381/)
6. Screenshot of skillsets 10-12


### Assignment Screenshots:

*Screenshot of LIS4381 Portal (Main Page):*

![LIS4381 Portal (Main Page)Screenshot](img/main.png)

| Screenshot of Failed Validation | Screenshot of Passed Validation |
| ---------- | ---------- |
| ![Failed Validation SS](img/failed_v.png) | ![Passed Validation SS](img/passed_v.png) |


### Links

Local LIS4381 Web App: [Local Host](http://localhost:8080/repos/lis4381/)

#### Skillset 10-12 Screenshots:

*Screenshot of Skillset 10: Array List:*

![Skillset 10 Array List Screenshot](img/q10_Array_List.png)

*Screenshot of Skillset 11: Alpha Numeric Special:*

![Skillset 11 Alpha Numeric Special Screenshot](img/q11_Alpha_Numeric_Special.png)

*Screenshot of Skillset 12: Temperature Conversion:*

![Skillset 12 Temperature Conversion Screenshot](img/q12_Temperature_Conversion.png)

